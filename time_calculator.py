def time_formatter(time):
	#formatting the dict to a string
	string_time = str(time['hours']) + ":"

	if time['minutes'] < 10:
		string_time += '0' + str(time['minutes'])
	else:
		string_time += str(time['minutes'])
	string_time += ' ' + time['period']
	if time['week_d']:
		string_time += ', ' + time['week_d']
	if time['days_passed'] == 1:
		string_time += ' (next day)'
	elif time['days_passed'] > 1:
		string_time += ' (' + str(time['days_passed']) + " days later)"
	return string_time

def add_time(start, duration, weekday = ""):
	#initializing variables
	days_of_week = [
		"Sunday",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday"
	]
	time = {
		'hours': 0,
		'minutes': 0,
		'period': "",
		'days_passed': 0,
		'week_d': ""
	}

	#spliting start time
	time_part, period = start.split()
	c_hours, c_minutes = time_part.split(':')
	time['hours'] = int(c_hours)
	time['minutes'] = int(c_minutes)
	time['period'] = period

	#spliting duration time
	add_hours, add_minutes = duration.split(':')
	add_hours = int(add_hours)
	add_minutes = int(add_minutes)

	#setting week day
	week_index = 0
	if weekday:
		for day in days_of_week:
			if day.lower() == weekday.lower():
				time['week_d'] = day
				break
			week_index += 1
	#running the time machine
	while add_hours >= 0:
		while add_minutes > 0:
			#incrementing current time and decrementing rest time
			time['minutes'] += 1
			add_minutes -= 1

			#increment hours and reset minutes
			if time['minutes'] == 60:
				time['hours'] += 1
				time['minutes'] = 0

			#change the period state
			if time['hours'] == 12 and time['minutes'] == 00:
				if time['period'] == 'AM':
					time['period'] = 'PM'
				#one day passed
				else:
					time['period'] = 'AM'
					time['days_passed'] += 1

					#changing the week day state
					if weekday:
						week_index += 1
						if (week_index == 7):
							week_index = 0
						time['week_d'] = days_of_week[week_index]

			#change the hours
			if time['hours'] == 13:
				time['hours'] = 1

		add_hours -= 1
		add_minutes = 60
	return time_formatter(time)


print(add_time('6:30 PM', '205:12'))